CC = clang

TARGET = main

CFLAGS = -std=c11 -O0 -g -Wall -Wpedantic -Wextra -Werror -lpthread

all:
	$(CC) $(CFLAGS) \
	crcular-buffer/crcular-buffer.c \
	thread-pool.c \
	main.c -o $(TARGET)

clean:
	rm $(TARGET)
