#include "circular-buffer.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

CircularBuffer* CircularBuffer_Create(size_t dataSize, size_t capacity)
{
	CircularBuffer* array = (CircularBuffer*)malloc(sizeof (CircularBuffer));
	assert(array);
	array->base = (void*)malloc(capacity * sizeof (dataSize));
	assert(array->base);
	array->front = 0;
	array->back = 0;
	array->dataSize = dataSize;
	array->capacity = capacity;
	array->size = 0;
	return array;
}

void CircularBuffer_Destroy(CircularBuffer* array)
{
	free(array->base);
	free(array);
}

static inline void* getDataAt(CircularBuffer* array, size_t index)
{
	return (char*)array->base + (array->dataSize * index);
}

void* CircularBuffer_Front(CircularBuffer* array)
{
	return getDataAt(array, array->front);
}

void* CircularBuffer_Back(CircularBuffer* array)
{
	if (array->back == 0) {
		return getDataAt(array, (array->capacity - 1));
	}
	return getDataAt(array, (array->back - 1));
}

void CircularBuffer_PushBack(CircularBuffer* array, void* data)
{
	memcpy(getDataAt(array, array->back), data, array->dataSize);
	array->back = (array->back + 1) % array->capacity;
	array->size++;
}

void CircularBuffer_PopFront(CircularBuffer* array)
{
	array->front = (array->front + 1) % array->capacity;
	array->size--;
}
