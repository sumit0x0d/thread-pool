#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <pthread.h>

#include <stdbool.h>
#include <stddef.h>

typedef struct ThreadPool {
	pthread_t* thread;
	size_t threadCount;
	struct CircularBuffer* queue;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	bool active;
} ThreadPool;

ThreadPool* ThreadPool_Create(size_t capacity, size_t threadCount);
void ThreadPool_Destroy(ThreadPool* threadPool);
bool ThreadPool_Insert(ThreadPool* threadPool, void (*function)(void*), void* argument);
void ThreadPool_Wait(ThreadPool* threadPool);
void ThreadPool_Pause(ThreadPool* threadPool);
void ThreadPool_Resume(ThreadPool* threadPool);

#endif
