#include "circular-buffer/circular-buffer.h"
#include "thread-pool.h"

#include <pthread.h>

#include <assert.h>
#include <stdlib.h>
#include <string.h>

static void* startRoutine(ThreadPool* threadPool)
{
	Task task;
	while(((ThreadPool*)threadPool)->active) {
		pthread_mutex_lock(&((ThreadPool*)threadPool)->mutex);
		pthread_cond_wait(&((ThreadPool*)threadPool)->cond, &((ThreadPool*)threadPool)->mutex);
		if(((ThreadPool*)threadPool)->queue->size) {
			task = CircularBuffer_Front(((ThreadPool*)threadPool)->queue);
			CircularBuffer_PopFront(((ThreadPool*)threadPool)->queue);
			task.function(task.argument);
		}
	}
	return NULL;
}

ThreadPool* ThreadPool_Create(size_t capacity, size_t threadCount)
{
	ThreadPool* threadPool = (ThreadPool*)malloc(sizeof (ThreadPool));
	assert(threadPool);
	threadPool->thread = (pthread_t*)malloc(threadCount * sizeof (pthread_t));
	assert(threadPool->thread);
	for(size_t i = 0; i < threadCount; i++) {
		pthread_create(threadPool->thread + i, NULL, startRoutine, threadPool);
	}
	threadPool->queue = CircularBuffer_Create(sizeof (pthread_t*), capacity);
	pthread_mutex_init(&threadPool->mutex, NULL);
	pthread_cond_init(&threadPool->cond, NULL);
	threadPool->active = true;
	threadPool->threadCount = threadCount;
	return threadPool;
}

void ThreadPool_Destroy(ThreadPool* threadPool)
{
	pthread_mutex_lock(&threadPool->mutex);
	pthread_cond_destroy(&threadPool->cond);
	pthread_mutex_destroy(&threadPool->mutex);
	free(threadPool->thread);
	CircularBuffer_Destroy(threadPool->queue);
	free(threadPool);
}

bool ThreadPool_Insert(ThreadPool* threadPool, void (*function)(void*), void* argument)
{
	if(threadPool->queue->size == threadPool->queue->capacity) {
		return false;
	}
	pthread_mutex_lock(&threadPool->mutex);
	/* Task task; */
	/* task.function = function; */
	/* task.argument = argument; */
	CircularBuffer_PushBack(threadPool->queue, function);
	pthread_cond_signal(&threadPool->cond);
	pthread_mutex_unlock(&threadPool->mutex);
	return true;
}

void ThreadPool_Wait(ThreadPool* threadPool)
{
	return;
}

void ThreadPool_Pause(ThreadPool* threadPool)
{
	return;
}

void ThreadPool_Resume(ThreadPool* threadPool)
{
	return;
}
